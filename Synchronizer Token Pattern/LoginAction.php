<?php
/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 03/10/2018
 * Time: 12:26
 */

require_once 'Token.php';


//user login form submit methode
if (isset($_POST['submitlogin'])){

    //start the session
    session_start();

    //save user session id to coockie
    $cookie_name = "userloginsession";
    $cookie_value = session_id();
    setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");

    //genarate csrf token and save it with session id
    $CSRF_TOKEN=Token::random_string(32);
    $myfile = fopen("sessionmapperfile.txt", "w") or die("Unable to open file!");
    $txt = $CSRF_TOKEN.",".$cookie_value;
    fwrite($myfile, $txt);
    fclose($myfile);

    //Redirect user to page consist with form
    header("Location: http://localhost/csrfimpl/Synchronizer Token Pattern/SampleFormPage.php");
    die();
}