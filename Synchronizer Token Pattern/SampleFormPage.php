<?php
/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 03/10/2018
 * Time: 13:18
 */

?>

<!DOCTYPE HTML >
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>csrf</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

<div style="width: 29%;border: 1px solid; padding: 25px;  margin-top: 8%;" class="container">
    <!--Sample form to submit recived CSRF tokens-->
    <form action="SampleFormAction.php" method="POST">
        <h2>Sample Form</h2>
        <input name="hiddencsrf" hidden id="inputvalue"  >
        <button type="submit" name="submitsample" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>

<!--calling to the CsrfTokensender methode that recive session cookie and send  csrf token and set CSRF token to hidden input field-->
<script type="text/javascript">
    function GetCsrf() {
        $.ajax({ url: 'CsrfTokenSender.php',
            data: {Cookiename: 'userloginsession'},
            type: 'post',
            success: function(output) {
                document.getElementById("inputvalue").value = output;
            }
        });
    }
    window.onload = GetCsrf;
</script>
</html>
