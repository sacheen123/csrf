<?php
/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 03/10/2018
 * Time: 15:47
 */

//Sample form submit methode
if (isset($_POST['submitsample'])){

    //save recived CSRF token to variable
    $RecivedCSRF=$_POST['hiddencsrf'];

    //get the session idintifier that save as cookie
    $sessionID=$_COOKIE['userloginsession'];

    //read from the file to take the mapped sessionid and the csrftoken
    $myfile = fopen("sessionmapperfile.txt", "r") or die("Unable to open file!");
    $data=fread($myfile,filesize("sessionmapperfile.txt"));
    fclose($myfile);
    $myString = $data;
    $myArray = explode(',', $myString);
    $mappersessionId=$myArray[1];

    //check the sessionid save in the cooki and the sessionid save in mapped file
    if($sessionID==$mappersessionId){
        //get the mapped file csrf token
        $mappercsrfID=$myArray[0];
        //check the mapped file csrf token and recive csrf token compatibility
        if($mappercsrfID==$RecivedCSRF){
          echo "<script>alert('Success!! CSRF token matched')</script>";
        }else{
            echo "<script>alert('Fail!! CSRF token not matched')</script>";
        }
    }else{
        print_r("Requested session idintifier not available");
        die();
    }

}