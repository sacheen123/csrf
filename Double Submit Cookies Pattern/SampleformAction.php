<?php
/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 03/10/2018
 * Time: 15:47
 */

//Sample form submit methode
if (isset($_POST['submitsample'])){

    //save recived CSRF token from the body
    $RecivedCSRF=$_POST['hiddencsrf'];

    //get the CSRF token that saved in cookies
    $CookieCSRF=$_COOKIE['usercsrftoken'];

    //check the recived CSRF token and CSRF token that saved in cookies
    if($RecivedCSRF==$CookieCSRF){
         echo "<script>alert('Success!! CSRF token matched')</script>";
         die();
    }else{
        echo "<script>alert('Fail!! CSRF token not matched')</script>";
        die();
    }

}