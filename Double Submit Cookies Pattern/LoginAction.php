    <?php
/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 03/10/2018
 * Time: 12:26
 */

require_once 'Token.php';


//user login form submit methode
if (isset($_POST['submitlogin'])){

    //start the session
    session_start();

    //save user session id to coockie
    $cookie_name_session = "userloginsession";
    $cookie_value_ssession = session_id();
    setcookie($cookie_name_session, $cookie_value_ssession, time() + (86400 * 30), "/");

    //genarate csrf token and save to a cookie
    $cookie_name_csrf = "usercsrftoken";
    $cookie_value_csrf = Token::random_string(32);
    setcookie($cookie_name_csrf, $cookie_value_csrf, time() + (86400 * 30), "/");


    //Redirect user to page consist with form
    header("Location: http://localhost/csrfimpl/Double Submit Cookies Pattern/SampleFormPage.php");
    die();
}